USE Labor_SQL;

SELECT trip_no, town_from, town_to FROM Trip
WHERE town_from = 'London' OR town_to = 'London'
ORDER BY time_out;

SELECT trip_no, plane, town_from, town_to FROM Trip
WHERE  plane ='TU-154'
ORDER BY time_out DESC;

SELECT trip_no, plane, town_from, town_to FROM Trip
WHERE plane != 'IL-86'
ORDER BY  plane;

SELECT trip_no, town_from, town_to FROM Trip
WHERE town_from != 'Rostov' AND town_to != 'Rostov'
ORDER BY time_out;

SELECT * FROM Trip
WHERE hour(time_in) between 12 AND 17;

SELECT * FROM Trip
WHERE hour(time_out) between 17 AND 23;

SELECT date FROM Pass_in_trip
WHERE place RLIKE '^1';

SELECT  date FROM Pass_in_trip
WHERE place RLIKE 'c$';

SELECT SUBSTRING_INDEX(name, ' ', -1) AS lastname from Passenger
WHERE name LIKE '% C%';

SELECT SUBSTRING_INDEX(name, ' ', -1) AS lastname from Passenger
WHERE name   NOT LIKE '% J%';

SELECT  DISTINCT name FROM Company, Trip
WHERE Trip.ID_comp = Company.ID_comp  AND plane = 'Boeing';

SELECT name, date FROM Passenger, Pass_in_trip
WHERE Passenger.ID_psg = Pass_in_trip.ID_psg;
