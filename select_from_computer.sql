USE Labor_SQL;

SELECT type, maker FROM Product
WHERE type = "laptop"
ORDER BY maker;

SELECT model, ram, screen, price FROM Laptop
WHERE price > 1000
ORDER BY ram DESC, price;

SELECT * FROM Printer
WHERE color = "y"
ORDER BY price;

SELECT model, speed, hd, cd, price FROM PC
WHERE (cd = "12X" OR cd = "24x") AND price < 600
ORDER BY speed DESC;

SELECT * FROM PC
WHERE speed > 500 AND price < 800
ORDER BY price DESC;

SELECT * FROM Printer
WHERE price < 300 and type = "Matrix"
ORDER BY type;

SELECT model, speed FROM PC
WHERE price BETWEEN 400 AND 600
ORDER BY hd;

SELECT PC.model, speed,hd FROM PC, Product
WHERE (maker ="A") AND (hd = 10 OR hd = 20) AND (PC.model = Product.model)
ORDER BY speed;

SELECT model, speed, hd, price FROM Laptop
WHERE screen > 12
ORDER BY price DESC;

SELECT model, type, price FROM Printer
WHERE price < 300
ORDER BY type DESC;

SELECT model, ram, price FROM PC
WHERE ram> 64
ORDER BY hd;

SELECT model, speed, price FROM PC
WHERE speed BETWEEN 500 AND 750
ORDER BY hd DESC;

SELECT  DISTINCT model FROM PC
WHERE   model  LIKE '%1%1%';

SELECT maker,type, speed, hd FROM Product, PC
WHERE hd > 8 AND Product.model = PC.model;

SELECT   DISTINCT maker FROM Product, PC
WHERE speed > 600 AND Product.model = PC.model;

SELECT   DISTINCT maker FROM Product, PC
WHERE speed < 500 AND Product.model = PC.model;

SELECT   DISTINCT One.model, Two.model, One.ram, One.hd FROM  Laptop One, Laptop Two
WHERE One.hd = Two.hd AND
      One.ram = Two.ram AND
      One.code != Two.code
ORDER BY  One.model DESC, Two.model;

SELECT DISTINCT Product.model, maker FROM Product, PC
WHERE price < 600 AND PC.model = Product.model;

SELECT DISTINCT  maker, Product.model FROM Product, Printer
WHERE price > 300 AND Printer.model = Product.model;

SELECT maker, PC.model, price FROM PC, Product
WHERE PC.model = Product.model;

SELECT maker, type, Product.model, speed FROM Product, Laptop
WHERE Laptop.model = Product.model AND Laptop.speed > 600;

