USE Labor_SQL;

SELECT name, class FROM Ships
ORDER BY name;

SELECT * FROM Classes
WHERE country = 'Japan'
ORDER BY type DESC;

SELECT name, launched FROM Ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC;

SELECT ship, battle, result FROM Outcomes
WHERE result != 'sunk' AND battle = 'Guadalcanal'
ORDER BY ship DESC;

SELECT ship, battle, result FROM Outcomes
WHERE result = 'sunk'
ORDER BY ship DESC;

SELECT class, displacement FROM Classes
WHERE displacement > 40000
ORDER BY type;

SELECT name FROM Ships
WHERE name LIKE 'W%n';

SELECT name FROM Ships
WHERE name LIKE '%e%e%';

SELECT name, launched FROM Ships
WHERE name NOT RLIKE 'a$';

SELECT name FROM Battles
WHERE name LIKE '% %' AND  name NOT RLIKE 'c$';

SELECT  DISTINCT One.country, One.class class_bb, Two.class class_bc  FROM Classes One, Classes Two
WHERE One.country = Two.country AND  One.type = 'bb' AND Two.type = 'bc' AND One.class != Two.class;

SELECT name, displacement FROM Classes, Ships
WHERE Classes.class = Ships.class;

SELECT ship, battle, date FROM Battles, Outcomes
WHERE Battles.name = Outcomes.battle AND result = 'OK';

SELECT name, country FROM Ships,Classes
WHERE Ships.class = Classes.class;
